﻿using System.Collections.Generic;
using System.Threading.Tasks;
using XboxOneHub.Modelo;

namespace XboxOneHub.Controlador
{
    class FuncionesTwitter
    {
        public static async Task<string> TimelineUsuario(string usuario)
        {
            PeticionesApiTw peticionesApiTw = new PeticionesApiTw();
            var task = await peticionesApiTw.timeline_de_usuario(usuario);
            return task;
        }
        
        public static async Task<List<Tweet>> BuscarTeewts(bool refresh, bool newer)
        {
            PeticionesApiTw peticionesApiTw = new PeticionesApiTw();
            var task = await peticionesApiTw.TweetListSearch(refresh, newer);
            return task;
        }

    }
}
