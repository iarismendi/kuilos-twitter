﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using XboxOneHub.Controlador;
using XboxOneHub.GUI;
using XboxOneHub.Modelo;

namespace XboxOneHub
{
    public partial class MainPage : PhoneApplicationPage
    {
        private List<Tweet> tweetList, tweetListSource;
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            hideAlerts();
            ObtenerLista(false, false);
            tweetListSource = new List<Tweet>();
        }

        private void ActualizarTiempo()
        {
            if (tweetListSource != null)
                for (int i = 0; i < tweetListSource.Count; i++)
                {
                    TweetItemControl tic = (TweetItemControl)listBoxTeewts.Items[i + 1];
                    tic.RefreshTime(tweetListSource[i].CreatedAt);
                    listBoxTeewts.Items[i + 1] = tic;
                }
        }

        private async void ObtenerLista(bool refresh, bool newer)
        {
            textLoading.Visibility = System.Windows.Visibility.Visible;

            tweetList = await FuncionesTwitter.BuscarTeewts(refresh, newer);
            int i = 1;
            if (tweetList == null)
            {
                textLoading.Visibility = System.Windows.Visibility.Collapsed;
                textAlertaRojo.Visibility = System.Windows.Visibility.Visible;
                textAlertaRojo.Text = "Sorry, there is a connection error";
            }
            else if (tweetList.Count == 0)
            {
                textLoading.Visibility = System.Windows.Visibility.Collapsed;
                textAlertaRojo.Visibility = System.Windows.Visibility.Visible;
                textAlertaRojo.Text = "There are no more news to load";
            }
            else
            {

                foreach (Tweet tweet in tweetList)
                {
                    TweetItemControl items = new TweetItemControl(tweet);
                    if (newer)
                    {
                        listBoxTeewts.Items.Insert(i, items);
                        tweetListSource.Insert(i - 1, tweet);
                    }
                    else
                    {
                        listBoxTeewts.Items.Add(items);
                        tweetListSource.Add(tweet);
                    }
                    i++;
                }
                listBoxTeewts.Items.Remove(buttonLoadMore);
                listBoxTeewts.Items.Insert(listBoxTeewts.Items.Count, buttonLoadMore);
                textAlertaRojo.Visibility = System.Windows.Visibility.Collapsed;
                textLoading.Visibility = System.Windows.Visibility.Collapsed;
                textAlertaVerde.Visibility = System.Windows.Visibility.Visible;
                textAlertaVerde.Text = tweetList.Count + " news loaded";
            }
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            ActualizarTiempo();
            hideAlerts();
            ObtenerLista(false, true);
        }

        private void listBoxTeewts_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            hideAlerts();
        }

        private void listBoxTeewts_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (listBoxTeewts.SelectedIndex - 1 >= 0)
                if (tweetListSource[listBoxTeewts.SelectedIndex - 1].Url != null)
                {
                    Uri uri = new Uri("/GUI/TweetDetails.xaml?url=" + tweetListSource[listBoxTeewts.SelectedIndex - 1].Url, UriKind.Relative);
                    NavigationService.Navigate(uri);
                }
        }

        private void buttonLoadMore_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            ActualizarTiempo();
            hideAlerts();
            ObtenerLista(true, false);
        }

        private void hideAlerts()
        {
            textAlertaRojo.Visibility = System.Windows.Visibility.Collapsed;
            textAlertaVerde.Visibility = System.Windows.Visibility.Collapsed;
            textLoading.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void ApplicationBarIconButton_Click_1(object sender, EventArgs e)
        {
            Uri uri = new Uri("/GUI/SettingsPage.xaml", UriKind.Relative);
            NavigationService.Navigate(uri);
        }
    }
}