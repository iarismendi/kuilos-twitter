﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;

namespace XboxOneHub.Modelo
{
    class Servicios
    {
        public string GetNonce()
        {
            Random random = new Random();
            int nonce = random.Next(1000000000);
            return nonce.ToString();
        }

        public string GetTimeStamp()
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds).ToString();
        }

        public async Task<string> Respuesta(string url, string postData)
        {
            try
            {
                HttpClient cliente = new HttpClient();
                cliente.MaxResponseContentBufferSize = int.MaxValue;
                cliente.DefaultRequestHeaders.ExpectContinue = false;
                HttpRequestMessage request = new HttpRequestMessage();
                request.Content = new StringContent(postData);
                request.Method = new HttpMethod("POST");
                request.RequestUri = new Uri(url, UriKind.Absolute);
                request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                var response = await cliente.SendAsync(request);
                return await response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string GetSignature(string sigBaseString, string consumerSecretKey, string requestTokenSecretKey = null)
        {
            var signingKey = string.Format("{0}&{1}", consumerSecretKey, !string.IsNullOrEmpty(requestTokenSecretKey) ? requestTokenSecretKey : "");
            byte[] byteArr = System.Text.Encoding.UTF8.GetBytes(signingKey);
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            HMACSHA1 hmacsha1 = new HMACSHA1(byteArr);
            byte[] messageBytes = encoding.GetBytes(sigBaseString);
            byte[] hashmessage = hmacsha1.ComputeHash(messageBytes);

            string signature = Convert.ToBase64String(hashmessage);
            return signature;
        }

        public async Task<string> HttpService(string urlWithParams, string qParams, string url)
        {
            string nonce = GetNonce();
            string timeStamp = GetTimeStamp();
            try
            {
                HttpClient httpClient = new HttpClient();
                httpClient.MaxResponseContentBufferSize = int.MaxValue;
                httpClient.DefaultRequestHeaders.ExpectContinue = false;
                HttpRequestMessage requestMsg = new HttpRequestMessage();
                requestMsg.Method = new HttpMethod("GET");
                requestMsg.RequestUri = new Uri(urlWithParams);
                string paramsBaseString = getParamsBaseString(qParams, nonce, timeStamp);
                string sigBaseString = "GET&";
                sigBaseString += Uri.EscapeDataString(url) + "&" + Uri.EscapeDataString(paramsBaseString);

                string signature = GetSignature(sigBaseString, Claves.consumerSecretKey, Claves.accessTokenKey);
                string data = "oauth_consumer_key=\"" + Claves.consumerKey
                              +
                              "\", oauth_nonce=\"" + nonce +
                              "\", oauth_signature=\"" + Uri.EscapeDataString(signature) +
                              "\", oauth_signature_method=\"HMAC-SHA1\", oauth_timestamp=\"" + timeStamp +
                              "\", oauth_token=\"" + Claves.accessToken +
                              "\", oauth_verifier=\"" + "N/A" +
                              "\", oauth_version=\"1.0\"";
                requestMsg.Headers.Authorization = new AuthenticationHeaderValue("OAuth", data);
                var response = await httpClient.SendAsync(requestMsg);
                return await response.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// https://dev.twitter.com/docs/auth/creating-signature
        /// </summary>
        /// <param name="queryParameters">e.g., count=5&trim_user=true</param>
        /// <returns></returns>
        private string getParamsBaseString(string queryParamsString, string nonce, string timeStamp)
        {
            // these parameters are required in every request api
            var baseStringParams = new Dictionary<string, string>{
                {"oauth_consumer_key", Claves.consumerKey},
                {"oauth_nonce", nonce},
                {"oauth_signature_method", "HMAC-SHA1"},
                {"oauth_timestamp", timeStamp},
                {"oauth_token", Claves.accessToken},
                {"oauth_verifier", "N/A"},
                {"oauth_version", "1.0"},                
            };

            // put each parameter into dictionary
            var queryParams = queryParamsString
                                .Split('&')
                                .ToDictionary(p => p.Substring(0, p.IndexOf('=')), p => p.Substring(p.IndexOf('=') + 1));

            foreach (var kv in queryParams)
            {
                baseStringParams.Add(kv.Key, kv.Value);
            }

            // The OAuth spec says to sort lexigraphically, which is the default alphabetical sort for many libraries.
            var ret = baseStringParams
                .OrderBy(kv => kv.Key)
                .Select(kv => kv.Key + "=" + kv.Value)
                .Aggregate((i, j) => i + "&" + j);

            return ret;
        }
    }
}
