﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XboxOneHub.Modelo
{
    class PeticionesApiTw
    {
        private Servicios servicios = new Servicios();
        //private string querySearch = "from:ign%20OR%20from:gamespot%20OR%20from:kotaku%20OR%20from:polygon%20OR%20from:joystiq%20OR%20from:gamesradar%20OR%20from:gametrailers%20%22xbox%20one%22&count=20";
		private string querySearch = "from:ign%20OR%20from:gamespot%20OR%20from:kotaku%20OR%20from:polygon%20OR%20from:joystiq%20OR%20from:gamesradar%20OR%20from:gametrailers%20%22&count=20";
		
        public async Task<string> timeline_de_usuario(string usuario)
        {
            string nombre = "screen_name=" + usuario;
            string url = Claves.userTimeline;
            try
            {
                var qParams = nombre;
                var urlWithParams = url + "?" + nombre;
                var respuesta = await servicios.HttpService(urlWithParams, qParams, url);
                return respuesta;
            }
            catch (Exception Err)
            {
                throw;
            }
        }

        private async Task<string> SearchTweets(string query)
        {
            string url = Claves.searchTweets;
            try
            {
                var urlWithParams = url + "?" + query;
                var respuesta = await servicios.HttpService(urlWithParams, query, url);
                return respuesta;
            }
            catch (Exception Err)
            {
                throw;
            }
        }

        public async Task<List<Tweet>> TweetListSearch(bool more, bool newer)
        {
            string moreParams = "";
            if (more)
                moreParams = "&max_id=" + (App.MenorId - 1);
            if (newer)
                moreParams = "&since_id=" + (App.MayorId + 1);
            var qParams = "q=" + querySearch + moreParams;
            var respuesta = await SearchTweets(qParams);
            if (respuesta == "")
                return null;
            List<Tweet> tweetList = new List<Tweet>();
            string user;
            JObject tweetJsonUser, tweetJson;
            Tweet tweet;
            JObject statuses = JObject.Parse(respuesta);
            respuesta = statuses["statuses"].ToString();
            JArray TweetListJson = JArray.Parse(respuesta);
            JArray UrlListJson;
            for (int i = 0; i < TweetListJson.Count; i++)
            {
                tweetJson = JObject.Parse(TweetListJson[i].ToString());
                tweet = new Tweet();
                user = tweetJson["user"].ToString();
                tweetJsonUser = JObject.Parse(user);
                tweet.Name = tweetJsonUser["name"].ToString();
                tweet.ProfileImageUrl = tweetJsonUser["profile_image_url"].ToString();
                user = tweetJson["entities"].ToString();
                tweetJsonUser = JObject.Parse(user);
                user = tweetJsonUser["urls"].ToString();
                UrlListJson = JArray.Parse(user);
                if (UrlListJson.Count > 0)
                {
                    tweetJsonUser = JObject.Parse(UrlListJson[0].ToString());
                    tweet.Url = tweetJsonUser["url"].ToString();
                }
                tweet.Text = tweetJson["text"].ToString();
                tweet.Id = long.Parse(tweetJson["id"].ToString());
                tweet.CreatedAt = DateTime.ParseExact(tweetJson["created_at"].ToString(), "ddd MMM dd HH:mm:ss K yyyy", CultureInfo.CurrentCulture);
                tweetList.Add(tweet);
            }
            return tweetList;
        }
    }
}
