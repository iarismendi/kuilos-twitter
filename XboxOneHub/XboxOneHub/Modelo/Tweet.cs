﻿using System;
using System.Windows.Media.Imaging;

namespace XboxOneHub.Modelo
{
    public class Tweet: IComparable<Tweet>
    {
        private DateTime createdAt;
        private string name;
        private string text;
        private string profileImageUrl;
        private long id;
        private BitmapImage imagen;
        private string url;
        
        public string ProfileImageUrl
        {
            get { return profileImageUrl; }
            set { profileImageUrl = value; setImagen(); }
        }

        public long Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Url
        {
            get { return url; }
            set { url = value; }
        }

        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public DateTime CreatedAt
        {
            get { return createdAt; }
            set { createdAt = value; }
        }

        public void setImagen()
        {
            BitmapImage newImage = new BitmapImage();
            newImage.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
            newImage.UriSource = new Uri(profileImageUrl);
            imagen = newImage;
        }

        public BitmapImage Imagen
        {
            get { return imagen; }
        }

        public int CompareTo(Tweet other)
        {
            return this.CreatedAt.CompareTo(other.CreatedAt);
        }
    }
}
