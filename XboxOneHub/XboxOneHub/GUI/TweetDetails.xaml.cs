﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace XboxOneHub.GUI
{
    public partial class TweetDetails : PhoneApplicationPage
    {
        public TweetDetails()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            webBrowser.Navigated += (sender, args) =>
            {
                gridLoading.Visibility = System.Windows.Visibility.Collapsed;
            };

            webBrowser.Navigating += (sender, args) =>
            {
                gridLoading.Visibility = System.Windows.Visibility.Visible;
            };

            
            base.OnNavigatedTo(e);
            string url = "";
            if (NavigationContext.QueryString.TryGetValue("url", out url))
            {
                Uri uri = new Uri(url);
                webBrowser.Navigate(uri);
            }
        }


    }
}