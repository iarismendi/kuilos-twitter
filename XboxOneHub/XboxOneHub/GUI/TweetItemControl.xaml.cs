﻿using System;
using System.Windows.Controls;
using XboxOneHub.Modelo;

namespace XboxOneHub.GUI
{
    public partial class TweetItemControl : UserControl
    {
        public TweetItemControl()
        {
            InitializeComponent();
        }

        public TweetItemControl(Tweet tweet)
        {
            InitializeComponent();
            SetData(tweet);
        }

        public void RefreshTime(DateTime tdate)
        {
            DateTime date = DateTime.Now;
            TimeSpan span = date.Subtract(tdate);
            textCreatedAt.Text = DateString(span);
        }

        private void SetData(Tweet tweet)
        {
            DateTime date = DateTime.Now;
            TimeSpan span = date.Subtract(tweet.CreatedAt);
            textCreatedAt.Text = DateString(span);
            textName.Text = tweet.Name;
            textText.Text = tweet.Text;
            profileImage.Source = tweet.Imagen;
            SetearId(tweet.Id);
        }

        private void SetearId(long id)
        {
            if (App.MenorId == 0)
                App.MenorId = id;
            if (App.MayorId == 0)
                App.MayorId = id;
            if (App.MayorId < id)
                App.MayorId = id;
            if (App.MenorId > id)
                App.MenorId = id;
        }

        private string DateString(TimeSpan span)
        {
            if (span.Days >= 7)
                return (span.Days / 7) + " weeks ago";
            else if (span.Days >= 1)
                return span.Days + " days ago";
            else if (span.Hours >= 1)
                return span.Hours + " hours ago";
            else if (span.Minutes >= 1)
                return span.Minutes + " minutes ago";
            else
                return span.Seconds + " seconds ago";
        }
    }
}
